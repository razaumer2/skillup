<?php
// Heading
$_['heading_title']    = 'Products Import / Update Module ';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Products IMport / Update Module module!';
$_['text_edit']        = 'Edit Products IMport / Update Module Module';

// Entry
$_['entry_status']     = 'Status';
$_['entry_upload']     = 'Upload Xls file'; 

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Products IMport / Update Module module!';