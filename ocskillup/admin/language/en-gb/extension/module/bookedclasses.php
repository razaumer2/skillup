<?php
// Heading
$_['heading_title']    = 'Booked Classes';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Booked Classes module!';
$_['text_edit']        = 'Edit Booked Classes Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Booked Classes module!';