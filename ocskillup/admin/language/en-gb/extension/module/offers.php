<?php
// Heading
$_['heading_title']    = 'Offers';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified offers module!';
$_['text_edit']        = 'Edit offers Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify offers module!';