<?php
// Heading
$_['heading_title']    = 'Total Instructors/Students';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified dashboard Instructors/Students!';
$_['text_edit']        = 'Edit Dashboard Instructors/Students';
$_['text_view']        = 'View more...';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';
$_['entry_width']      = 'Width';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify dashboard Instructors/Students!';