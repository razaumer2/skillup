<?php
class ControllerExtensionModuleXlsupdate extends Controller {
	private $error = array();

	public function getImportedData(){
		$query=$this->db->query("SELECT * FROM ".DB_PREFIX."product ");
		echo count($query->rows);
	}
	public function importExcel(){
		require_once DIR_SYSTEM.'library/SimpleXLSX.php';
			if ( $xlsx = SimpleXLSX::parse($this->request->post['uploaded_file'])) {
					
							// Produce array keys from the array values of 1st array element
							$header_values = $rows = [];
							$headers=array();
							foreach ( $xlsx->rows() as $k => $r ) {
								if ( $k === 0 ) {
									$header_values = $r;
									continue;
								}
								$headers=$header_values;
								$rows[] = array_combine( $header_values, $r );
							}
							$json=array();

							if($this->request->post['action_choice']!=""&&$this->request->post['action_choice']=='all'){
									foreach ($rows as $key => $val) {
							
										$this->insertOrUpdateAll($val);
										//echo "processed ".$key." / ".count($rows)."</br>";

										
								
									}
									$this->session->data['success'] = "File updated successfully";
						//$this->response->redirect($this->url->link('extension/module/xlsupdate', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));

							}
						$json['success']='success';
						echo json_encode($json);
						$this->session->data['success'] = "File updated successfully";
						//$this->response->redirect($this->url->link('extension/module/xlsupdate', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
								
			    } else {
			        echo "Sorry, there was an error to parse you excel file. Hint: your file must be in xlsx format.";
			    }
	}

	public function index() {
		$this->load->language('extension/module/xlsupdate');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_xlsupdate', $this->request->post);
////////////////////////////////////////////////////////////////////////////////////////////////


			$target_dir = DIR_IMAGE;
			$target_file = $target_dir . basename($_FILES["upload_xls"]["name"]);
			$uploadOk = 1;
			$FileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			//if($FileType=='xlsx'){
			require_once DIR_SYSTEM.'library/SimpleXLSX.php';

			if(!isset($this->request->post['uploaded_file'])){
			    if (move_uploaded_file($_FILES["upload_xls"]["tmp_name"], $target_file)) {
			        
						if ( $xlsx = SimpleXLSX::parse(DIR_IMAGE. basename( $_FILES["upload_xls"]["name"]))) {

							// Produce array keys from the array values of 1st array element
							$header_values = $rows = [];
							$headers=array();
							foreach ( $xlsx->rows() as $k => $r ) {
								if ( $k === 0 ) {
									$header_values = $r;
									continue;
								}
								$headers=$header_values;
								$rows[] = array_combine( $header_values, $r );
							}

							$data['headers'] = $headers;
							$data['excel_values']=$rows;
							
							$data['uploaded_file']=DIR_IMAGE. basename( $_FILES["upload_xls"]["name"]);
							

			
			    } else {
			        echo "Sorry, there was an error to parse you excel file.";
			    }

			}
			}else{	



			}
		/*}else{
			$this->session->data['warning']="File Should be .xlsx!";
		}*/

////////////////////////////////////////////////////////////////////////////////////////////////

		

			$this->session->data['success'] = $this->language->get('text_success');

			//$this->response->redirect($this->url->link('extension/module/xlsupdate', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['warning'])) {
			$data['error_warning'] = $this->session->data['warning'];
		} else {
			$data['error_warning'] = '';
		}
$data['user_token']=$this->session->data['user_token'];
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		} else {
			$data['success'] = '';
		}
unset($this->session->data['success']);
unset($this->session->data['warning']);
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/xlsupdate', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/xlsupdate', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['module_xlsupdate_status'])) {
			$data['module_xlsupdate_status'] = $this->request->post['module_xlsupdate_status'];
		} else {
			$data['module_xlsupdate_status'] = $this->config->get('module_xlsupdate_status');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/xlsupdate', $data));
	}
	public function insertOrUpdateAll($val){
		

				$query=$this->db->query("SELECT product_id from ".DB_PREFIX."product WHERE model='".$val['barcode']."' ");
				if($query->num_rows){
				$this->db->query("UPDATE ".DB_PREFIX."product SET price='".$this->db->escape($val['price_2'])."',price_1='".$this->db->escape($val['price_1'])."', model='".$this->db->escape($val['barcode'])."' WHERE product_id='".$query->row['product_id']."' ");					
				$this->db->query("UPDATE ".DB_PREFIX."product_description SET name='".$this->db->escape($val['stock_name'])."' WHERE product_id='".$query->row['product_id']."' and language_id='1' ");		
						

				}else{
				$this->db->query("INSERT INTO ".DB_PREFIX."product SET price='".$this->db->escape($val['price_2'])."',price_1='".$this->db->escape($val['price_1'])."', model='".$this->db->escape($val['barcode'])."', quantity='10', status='1', minimum='1', date_added=NOW(),date_modified=NOW() ");
				$product_id=$this->db->getLastId();
				$this->db->query("INSERT INTO ".DB_PREFIX."product_description SET name='".$this->db->escape($val['stock_name'])."', language_id='1',description='".$this->db->escape($val['stock_name'])."', meta_title='".$this->db->escape($val['stock_name'])."',product_id='".$product_id."' ");
			
				
				$this->db->query("INSERT INTO ".DB_PREFIX."product_to_store SET product_id='".$product_id."',store_id='0' ");
				$this->db->query("INSERT INTO ".DB_PREFIX."product_to_layout SET product_id='".$product_id."',store_id='0', layout_id='0' ");
				
				$ifcatproexist=$this->db->query("SELECT product_id FROM ".DB_PREFIX."product_to_category where product_id='".$product_id."' and category_id='66' ");
				
				
			}
		
			}

	

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/xlsupdate')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}