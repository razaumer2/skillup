<?php
class ControllerExtensionModuleOffers extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/offers');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_offers', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['offers']=array();

		$offer=$this->db->query("SELECT * FROM ".DB_PREFIX."offers WHERE 1 ");
		if($offer->num_rows){
			foreach ($offer->rows as $key => $value) {
				$data['offers'][]=array(
							'name'	=> ucfirst($value['fname'])." ".ucfirst($value['lname']),
							'email'	=> $value['email'],
							'telephone'=>$value['telephone'],
							'company'	=>$value['company'],
							'action_approve'	=>$this->url->link('extension/module/offers/approved','user_token='. $this->session->data['user_token'],true),
							'action_disapprove'	=>$this->url->link('extension/module/offers/disapproved','user_token='. $this->session->data['user_token'],true)
				);
			}
			
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/offers', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/offers', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->post['module_offers_status'])) {
			$data['module_offers_status'] = $this->request->post['module_offers_status'];
		} else {
			$data['module_offers_status'] = $this->config->get('module_offers_status');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/offers', $data));
	}

	public function approved(){
		if(isset($this->request->get['email'])){
			$this->db->query("UPDATE ".DB_PREFIX."user SET status='1' WHERE email like '".$this->db->escape($this->request->get['email'])."' ");
		}

		$this->response->redirect($this->url->link('extension/module/offers', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		
	}

	public function disapproved(){
		if(isset($this->request->get['email'])){
			$this->db->query("UPDATE ".DB_PREFIX."user SET status='0' WHERE email like '".$this->db->escape($this->request->get['email'])."' ");
		}

		$this->response->redirect($this->url->link('extension/module/offers', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/offers')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}