<?php
class ControllerExtensionModuleBookedclasses extends Controller {
	public function index() {
		$this->load->language('extension/module/bookedclasses');

		if(null!==$this->config->get('module_bookedclasses_status')&&$this->config->get('module_bookedclasses_status')){
			$data['module_bookedclasses_status']=true;
		}else{
			$data['module_bookedclasses_status']=false;
		}

		return $this->load->view('extension/module/bookedclasses', $data);
	}
}