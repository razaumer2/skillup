<?php
class ControllerCommonOfferclass extends Controller {
	private $error = array();
	public function index() {
$this->document->setTitle("Offer Class");
		$this->document->setDescription("Offer Class What you have skills to teach others");
		$this->document->setKeywords("Offer Class");
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			if(isset($this->request->post['fname'])){
			$data['fname']=$this->request->post['fname'];
		}else{
			$data['fname']="";
		}
		if(isset($this->request->post['lname'])){
			$data['lname']=$this->request->post['lname'];
		}else{
			$data['lname']="";
		}

		if(isset($this->request->post['email'])){
			$data['email']=$this->request->post['email'];
		}else{
			$data['email']="";
		}

		if(isset($this->request->post['telephone'])){
			$data['telephone']=$this->request->post['telephone'];
		}else{
			$data['telephone']="";
		}

		if(isset($this->request->post['company'])){
			$data['company']=$this->request->post['company'];
		}else{
			$data['company']="";
		}

		if(isset($this->request->post['type'])){
			$data['type']=$this->request->post['type'];
		}else{
			$data['type']="";
		}

		if(isset($this->request->post['city'])){
			$data['city']=$this->request->post['city'];
		}else{
			$data['city']="";
		}

		if(isset($this->request->post['state'])){
			$data['state']=$this->request->post['state'];
		}else{
			$data['state']="";
		}
		if(isset($this->request->post['class_type'])){
			$data['class_type']=$this->request->post['class_type'];
		}else{
			$data['class_type']="";
		}

		if(isset($this->request->post['website'])){
			$data['website']=$this->request->post['website'];
		}else{
			$data['website']="";
		}

		$this->db->query("INSERT INTO `".DB_PREFIX."offers`( `fname`, `lname`, `email`, `password`, `type`, `city`, `state`, `company`, `telephone`, `class_type`, `website`) VALUES ('".$this->db->escape($data['fname'])."','".$this->db->escape($data['lname'])."','".$this->db->escape($data['email'])."','".$this->request->post['password']."','".$data['type']."','".$data['city']."','".$data['state']."','".$this->db->escape($data['company'])."','".$data['telephone']."','".$this->db->escape($data['class_type'])."','".$this->db->escape($data['website'])."')");

		//$this->emailShoot($data);
		$this->db->query("INSERT INTO `".DB_PREFIX."user` SET user_group_id='11',username='".$this->db->escape($data['fname'].rand())."', firstname='".$this->db->escape($data['fname'])."',email='".$this->db->escape($data['email'])."',lastname='".$this->db->escape($data['lname'])."',password='".$this->db->escape(md5($this->request->post['password']))."',status='0',date_added=NOW()");

$this->session->data['success'] = "Thanks! Your application has been received we will contact you soon after revise your application";

			$this->response->redirect($this->url->link('common/offerclass', 'token=' . $this->session->data['token'] . '&type=module', true));
		}
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		} else {
			$data['success'] = false;
		}

		
		

		$data['action']	=	$this->url->link("common/offerclass");

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/offerclass', $data));
	}
	protected function validate(){
		if (!$this->request->post['fname']) {
			$this->error['warning'] = "Your Firstname is required!";
		}
		if (!$this->request->post['lname']) {
			$this->error['warning'] = "Your Lastname is required!";
		}

		if (!$this->request->post['email']) {
			$this->error['warning'] = "Your valid Email is required!";
		}
		if (!$this->request->post['password']) {
			$this->error['warning'] = "Your Password is required!";
		}
		if (!$this->request->post['telephone']) {
			$this->error['warning'] = "Your Phone is required!";
		}
		if (!$this->request->post['telephone']) {
			$this->error['warning'] = "Your Phone is required!";
		}

		if (!$this->request->post['class_type']) {
			$this->error['warning'] = "Your Class Type is required!";
		}

		if (!$this->request->post['company']) {
			$this->error['warning'] = "Your Company name is required!";
		}

		return !$this->error;
	}
}