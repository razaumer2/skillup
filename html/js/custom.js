$(document).ready(function(){
	"use strict";
	

	/*==============================================================
   		DL Menu Script Start
   	============================================================== */
	if($('#dl-menu').length){
		$(function() {
			$( '#dl-menu' ).dlmenu({
				animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
			});
		});
	}
	if($('#dl_menu_bottom').length){
		$(function() {
			$('#dl_menu_bottom').dlmenu({
				animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
			});
		});
	}

	/*==================================================================
		Banner Slider Script
	=================================================================	*/	
	if($('.banner-slider').length){
		$('.banner-slider').slick({
			slidesToShow: 1,
			autoplay: true,
			autoplaySpeed: 2000,
			dots:false,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						infinite: true,
						dots: true
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
		  	]
		});
	}
	/*==================================================================
		Client Slider Script
	=================================================================	*/	  	  
	$('#keo_classes_slider').slick({
		centerMode: true,
		slidesToShow: 5,
		responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        centerMode: true,
		        slidesToShow: 3
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        centerMode: true,
		        slidesToShow: 1
		      }
		    }
		]
	});

	/*==================================================================
		Banner Slider Script
	=================================================================	*/	
	if($('#keo_testimonial_slider').length){
		$('#keo_testimonial_slider').slick({
			slidesToShow: 1,
			autoplay: true,
			autoplaySpeed: 2000,
			dots:false,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						infinite: true,
						dots: true
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
		  	]
		});
	}

	/*==================================================================
		Back to Top Script
	=================================================================	*/
	$(window).scroll(function() {
	    if ($(this).scrollTop()) {
	        $('.keo_topbtn').fadeIn();
	    } else {
	        $('.keo_topbtn').fadeOut();
	    }
	});
	if($('.keo_topbtn').length){
		$('.keo_topbtn').on("click",function() {		
			$('html, body').animate({scrollTop : 0},800);
			return false;
		});
	}
	/*==================================================================
		Back to Top Script
	=================================================================	*/
	// Hide Loading Box (Preloader)
	function handlePreloader() {
		if($('.preloader').length){
			$('.preloader').delay(500).fadeOut(500);
		}
	}
	// window on load function
	jQuery(window).on('load', function () {
		(function ($) {
			// add your functions
			handlePreloader();
		})(jQuery);
	});

	/*
	  =======================================================================
		  		Range Slider Script Script
	  =======================================================================
	*/
	if($('.slider-range').length){
		$( ".slider-range" ).slider({
		  range: true,
		  min: 0,
		  max: 500,
		  values: [ 50, 450 ],
		  slide: function( event, ui ) {
			$( ".amount" ).val( "DHS" + ui.values[ 0 ] + " - DHS" + ui.values[ 1 ] );
		  }
		});
		$( ".amount" ).val( "DHS" + $( ".slider-range" ).slider( "values", 0 ) +
		  " - DHS" + $( ".slider-range" ).slider( "values", 1 ) );
	}
});